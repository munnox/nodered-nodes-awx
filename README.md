# nodered-nodes-noderedapi

Building a AWX server interface node set. This will allow NodeRED eventually to take advantage of the the power of AWX and Ansible.

Right now it can read information about the major resource types.

It can also write data to launch Job Templates.

## Install

```bash
npm install munnox/nodered-nodes-awx
```

Nodered Dependancies

```json
{
    "nodered-nodes-awx": "github:munnox/nodered-nodes-awx#dev",
}
```

## Develop and test:

```bash
# Build the package
npm build
# create a tgz which can then be upload to the nodred server
npm pack

# Test docker container commands
# Run a server to test the module on
npm run testserver
# Remove the test server
npm run testremove
# Reset test server
npm run testrestart
# Check logs
npm run testlogs
```

Example flows can be found in the examples directory.
