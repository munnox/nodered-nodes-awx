
function isemptyobject(obj) {
    // From https://stackoverflow.com/questions/679915/how-do-i-test-for-an-empty-javascript-object
    var empty = (
        obj
        && Object.keys(obj).length === 0
        && Object.getPrototypeOf(obj) === Object.prototype
    );
    return empty;
}

module.exports = {
    "isemptyobject": isemptyobject,
}